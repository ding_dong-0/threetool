package com.tctel.gacs.service;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Slf4j
@Service
public class ForwardService {

    @Value(value = "${destination.relayserver}")
    String url;

    @Autowired
    RestTemplate restTemplate;

    public String sendToGACS(String data) {

        HttpHeaders headers = new HttpHeaders();
        //headers 存放请求头信息 请求案例
        headers.add("Content-Type", "application/json;charset=utf-8");

        HashMap<String, String> hashMap = new HashMap<>();

        hashMap.put("data", data);

        HttpEntity httpEntity = new HttpEntity(hashMap, headers);
        log.info("============发送的请求体===========");
        log.info(JSON.toJSONString(httpEntity));
        log.info("=================================");

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, httpEntity, String.class);

        String body = responseEntity.getBody();
        System.out.println(body);
        return "sucess";
    }


}
