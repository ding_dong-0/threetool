package com.tctel.gacs.common.util;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class HttpUtilZshjs {




    public static String returnResult() {
        JSONArray fieldValues = new JSONArray();
        JSONObject jsonObject1 = new JSONObject();

        jsonObject1.put("field","result");
        jsonObject1.put("value","success");
        jsonObject1.put("isCode",0);
        jsonObject1.put("codeValue","");

        fieldValues.add(jsonObject1);

        JSONArray data = new JSONArray();
        JSONObject sourceId = new JSONObject();
        sourceId.put("sourceId","222");
        sourceId.put("fieldValues",fieldValues);

        data.add(sourceId);

        JSONObject resultObject = new JSONObject();

        resultObject.put("code", "1");
        resultObject.put("msg", "OK");
        resultObject.put("data",data);


        JSONObject pageSON = new JSONObject();
        pageSON.put("pageSize", "10");
        pageSON.put("pageNo", "1");
        pageSON.put("total", "1");
        resultObject.put("page", pageSON);
        resultObject.put("sign", "1qqqqwwwwwwwwwwww");

        JSONObject rootjsonObject = new JSONObject();
        rootjsonObject.put("jsonrpc", "2.0");
        rootjsonObject.put("id", "1");

        rootjsonObject.put("result", resultObject);


        System.out.println(rootjsonObject.toJSONString());
        return rootjsonObject.toJSONString();



    }


}
