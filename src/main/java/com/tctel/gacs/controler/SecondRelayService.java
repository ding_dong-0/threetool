package com.tctel.gacs.controler;

import com.tctel.gacs.common.util.HttpUtilZshjs;
import com.tctel.gacs.service.ForwardService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;


@Slf4j
@RestController
public class SecondRelayService {


    @Resource
    ForwardService forwardService;


    @PostMapping("index")
    public String index(@RequestBody Map map) {
        log.info(map.toString());
        System.out.println(map);
        Map map1 = ((Map) map.get("params"));
        Object o1 = ((Map) map1.get("data")).get("condition");
        Object o2 = ((Map) map1.get("data")).get("dataObjId");

        log.info("接收到的加密文件字符串为：{}", o1);
        log.info("文件转发成功{} ",o2);
        return HttpUtilZshjs.returnResult();
    }


    @PostMapping("/recevemessage")
    public String send(@RequestBody Map map) throws IOException {
        System.out.println(map);
        String o = (String) map.get("data");
        log.info("接收到的加密文件字符串为：{}", o);
//        forwardService.sendToGACS(o);
        log.info("文件转发成功");
        return HttpUtilZshjs.returnResult();
    }

}
